Guest book by laravel
=================
 
### Как пользоваться: ###

**Клонируем себе содержимое этого репозитория**

```sh
$ git@bitbucket.org:WebMonkeySu/guestbook.git
$ composer install
```

**Копируем .env.example в .env, генерируем ключи, а так же настраиваем подключение к бд**
```sh
 php artisan key:generate
```

**Устанавливаем пакет NodeJS**
``` 
npm i
```

**Применяем миграции:**

```sh
php artisan migrate
```

Для модерирования сообщений /admin/message