import Vue from 'vue/dist/vue.js';
import VueResource from 'vue-resource/dist/vue-resource.js';
import Messages from './components/MessageList.vue';

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

new Vue({
    el: '#app',
    components : {Messages},
    methods: {
        updateResource(data){
            this.messageList = data
        }
    },
    ready : function () {
        console.log('Vue ready');
    }
});
