import Vue from 'vue/dist/vue.js';

Vue.component('post', {
    template: '<li class="list-group-item">\
    <i class="glyphicon glyphicon-chevron-up" @click="upvote" :class="{disabled: upvoted}"></i>\
    <span class="label label-primary">{{ votes }}</span>\
<i class="glyphicon glyphicon-chevron-down" @click="downvote" :class="{disabled: downvoted}"></i>\
    <a>{{ post.title }}</a>\
</li>',
    props: ['post'],
    data: function () {
        return {
            upvoted: false,
            downvoted: false
        };
    },
    methods: {
        upvote: function () {
            this.upvoted = !this.upvoted;
            this.downvoted = false;
        },
        downvote: function () {
            this.downvoted = !this.downvoted;
            this.upvoted = false;
        }
    },
    computed: {
        votes: function () {
            if (this.upvoted) {
                return this.post.votes + 1;
            } else if (this.downvoted) {
                return this.post.votes - 1;
            } else {
                return this.post.votes;
            }
        }
    }
});

var vm = new Vue({
    el: "#app",
    data: {
        posts: []
    },
    ready: function () {
        var self = this;
            console.log('asd');
        $.ajax({
            url: '@Url.Action("GetItems", "Settings")',
            method: 'GET',
            success: function (data) {
                self.Items = data;
            },
            error: function (error) {
                alert(JSON.stringify(error));
            }
        });
    }
});