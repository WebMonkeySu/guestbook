@extends('layouts.main')

@section('content')
    <div id="app">
        <messages></messages>
    </div>
    <script src="/js/main.js"></script>
@stop