@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Update message #{{$message->id}}</h4>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ action('Admin\Message\MessageController@update',['id' => $message['id']]) }}">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('is_visible') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Is visible</label>
                                <div class="col-md-6">
                                    <input type="checkbox" name="is_visible" {{ old('is_visible', $message->is_visible) == \App\Message::IS_VISIBLE_ON ? 'checked' : '' }}></input>
                                    @if ($errors->has('is_visible'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('is_visible') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">User name</label>
                                <div class="col-md-6">
                                    <input  disabled type="text" class="form-control" name="username" value="{{ old('username', $message->username) }}">
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input  disabled type="email" class="form-control" name="email" value="{{ old('email', $message->email) }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Text</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="message"> {{ old('message', $message->message) }}</textarea>
                                    @if ($errors->has('message'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-floppy-o"></i> Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop