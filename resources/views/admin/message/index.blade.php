@extends('layouts.main')

@section('content')
    <p>
        <a href="{{ action('Admin\Message\MessageController@create') }}" class="btn btn-success">Create</a>
    </p>
    @unless($messages->count())
        Message not found.
        @else
            <table class="table table-bordered table-striped">
                <thead>
                <th>ID</th>
                <th>Is visible</th>
                <th>Username</th>
                <th>Email</th>
                <th>Message</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($messages as $message)
                    <tr>
                        <td>{{ $message->id }}</td>
                        <td>{{ $message->is_visible == \App\Message::IS_VISIBLE_ON ? 'Yes' : 'No' }}</td>
                        <td>{{ $message->username }}</td>
                        <td>{{ $message->email }}</td>
                        <td>{{ $message->message }}</td>
                        <td>
                            <a href="{{ action('Admin\Message\MessageController@update',['id' => $message['id']]) }}"><i class="fa fa-pencil-square-o"></i></a>
                            <a href="{{ action('Admin\Message\MessageController@delete',['id' => $message['id']]) }}"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endunless

@stop