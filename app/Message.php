<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 * @package App
 * @property string $username
 */
class Message extends Model
{
    const IS_VISIBLE_OFF = 0;
    const IS_VISIBLE_ON = 1;
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'username', 'email', 'message',
    ];
    //  'is_visible', 'user_id',

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeVisible($query)
    {
        return $query->where('is_visible', 1);
    }
}
