<?php

namespace App\Http\Controllers\Admin\Message;

use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::with('author')
            ->orderBy('id', SORT_DESC)
            ->take(25)
            ->get();

        return view('admin.message.index', ['messages' => $messages]);

    }

    public function create()
    {

    }

    public function update(Request $request, $id)
    {
        $message = $this->findModel($id);
        $data = $request->all();
        if(!empty($data) && $this->validator($data)) {
            $message->is_visible = !empty($data['is_visible']) && $data['is_visible'] == 'on' ? Message::IS_VISIBLE_ON : Message::IS_VISIBLE_OFF;
            $message->message = $data['message'];
            $message->save();
        }
        return view('admin.message.update', ['message' => $message]);
    }

    public function delete($id)
    {
        $this->findModel($id)->delete();
        return redirect()->back();
    }

    
    public function view()
    {

    }

    /**
     * @param $id
     * @return mixed
     */
    protected function findModel($id)
    {
        if( ($model = Message::where('id', intval($id))->first()) === null) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|alpha_num|max:255',
            'email' => 'nullable|email',
            'message' => 'required|string'
        ]);
    }
}
