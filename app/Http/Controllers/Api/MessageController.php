<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 17.02.2017
 * Time: 11:48
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public function rules()
    {
        return [
            'name' => 'required|alpha_num|max:255',
            'email' => 'email|max:255|unique:users'
        ];
    }

    public function index()
    {
        /** @var Builder $message */
        $query = Message::visible()
            ->with(['author'])
            ->orderBy('id', SORT_DESC)
            ->take(10);


        if(!empty($_GET['o']) && $_GET['o'] == 'self' && Auth::check()) {
            $query->where(['user_id' => Auth::id()]);
        }

        return JsonResponse::create([
            'isGuest' => !Auth::check(),
            'messages' => $query->get()
        ]);
    }

    public function save(Request $request)
    {

        $data = $request->all();
        $validation = $this->validator($data);

        if($validation->fails()) {
            return JsonResponse::create($validation->errors());
        } else {
            $data['user_id'] = Auth::check() ? intval(Auth::id()) : 0;
            $message = Message::create($data);
            return JsonResponse::create($message->wasRecentlyCreated ? 1 : 0);
        }

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|alpha_num|max:255',
            'email' => 'nullable|email',
            'message' => 'required|string'
        ]);
    }
}
