<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{

    public function index()
    {
        /** @var Builder $message */
        $message = Message::visible()
        ->with(['author'])
        ->orderBy('id', SORT_DESC)
        ->take(10)
        ->get();

        return view('contact.index', ['messages' => $message]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
}
