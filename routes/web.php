<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', []);
});
// Маршруты аутентификации...
Route::get('auth/login', 'Auth\LoginController@getLogin');
Route::post('auth/login', 'Auth\LoginController@postLogin');
Route::get('auth/logout', 'Auth\LoginController@logout');

Route::get('auth/signup', 'Auth\SignupController@getSignup');
Route::post('auth/signup', 'Auth\SignupController@postSignup');

Route::group(['prefix' => 'api/v1'], function() {
    Route::post('message/save', 'Api\MessageController@save');
    Route::resource('message', 'Api\MessageController');
});
Route::get('/contact', 'ContactController@index');


Route::get('admin/message', 'Admin\Message\MessageController@index');
Route::get('admin/message/create', 'Admin\Message\MessageController@create');
Route::get('admin/message/update/{id}', 'Admin\Message\MessageController@update');
Route::post('admin/message/update/{id}', 'Admin\Message\MessageController@update');
Route::get('admin/message/delete/{id}', 'Admin\Message\MessageController@delete');
